const BASE_URL = "http://18.207.246.185:3000"


// SCENARIO 2
export async function uploadFile(file) {
  const formData = new FormData()
  formData.append("file", file)

  try {
    const response = await fetch(`${BASE_URL}/api/bulk-update-and-cache`, {
      method: "POST",
      body: formData,
    })

    if (response.ok) {
      const data = await response.json()
      return data
    } else {
      throw new Error("Failed to upload file")
    }
  } catch (error) {
    throw error
  }
}

// SCENARIO 2
export async function updateAddresses(addresses) {
  console.log("frontend data : " + JSON.stringify(addresses));
  try {
    const response = await fetch(`${BASE_URL}/api/update-address`, {
      method: "POST",
      body: JSON.stringify(addresses), // Convert the array to JSON
      headers: {
        'Content-Type': 'application/json' // Set the content type to JSON
      },
    });

    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      throw new Error("Failed to upload addresses");
    }
  } catch (error) {
    throw error;
  }
}



// SCENARIO 1
export async function getCachedData() {
  try {
    const response = await fetch(`${BASE_URL}/api/cached-addresses`)

    if (response.ok) {
      const data = await response.json()
      return {
        addresses: data.addresses
      }
    } else {
      await getMongoDBData()
    }
  } catch (error) {
    throw error
  }
}

// SCENARIO 1
export async function getMongoDBData() {
  try {
    const response = await fetch(`${BASE_URL}/api/mongo-addresses`);

    if (response.ok) {
      const data = await response.json();
      return {
        addresses: data.addresses
      };
    } else {
      console.error(`Error: ${response.status} - ${response.statusText}`);
      throw new Error("Failed to fetch cached data");
    }
  } catch (error) {
    console.error("An error occurred:", error);
    throw new Error("Failed to fetch data.");
  }
}

// SCENARIO 5
export async function refreshCachedData() {
  try {
    const response = await fetch(`${BASE_URL}/api/refresh-cached-data`, {
      method: "GET",
    })

    if (response.ok) {
      console.log("Cached data refreshed successfully")
    } else {
      throw new Error("Failed to refresh cached data")
    }
  } catch (error) {
    throw error
  }
}

// SCENARIO 3
export async function clearCachedData() {
  try {
    const response = await fetch(`${BASE_URL}/api/clear-all-cached-data`, {
      method: "GET",
    })

    if (response.ok) {
      console.log("All cached data cleared successfully")
      return { status: response.status }
    } else {
      throw new Error("Failed to clear all cached data")
    }
  } catch (error) {
    throw error
  }
}

// SCENARIO 4
export const clearAllMongoDBData = async () => {
  try {
    const response = await fetch(`${BASE_URL}/api/clear-all-mongodb-data`)
    await clearCachedData();
    return response
  } catch (error) {
    throw error
  }
}
