import React, { useState } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import "./styles.css"

function FileUpload({ onFileUpload }) {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadProgress, setUploadProgress] = useState(0);

  const BASE_URL = "http://localhost:3000";

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file && file.type === "text/csv") {
      setSelectedFile(file);
    } else {
      setSelectedFile(null);
    }
  };

  const handleUpload = async () => {
    if (selectedFile) {
      const formData = new FormData();
      formData.append("file", selectedFile);

      const xhr = new XMLHttpRequest();

      await xhr.upload.addEventListener("progress", (e) => {
        if (e.lengthComputable) {
          const progressPercentage = (e.loaded / e.total) * 100;
          setUploadProgress(progressPercentage);
        }
      });

      xhr.open("POST", `${BASE_URL}/api/bulk-update-and-cache`, true);
      xhr.send(formData);

      xhr.onload = () => {
        if (xhr.status === 201) {
          console.log("File uploaded successfully");
        } else {
          console.error("File upload failed");
        }
        window.location.reload();
      };
    }
  }

  return (
    <div>
      <input type="file" accept=".csv" onChange={handleFileChange} />
      {uploadProgress > 0 && (
        <ProgressBar
          now={uploadProgress}
          label={`${uploadProgress.toFixed(2)}%`}
        />
      )}
      <button
        className={`gradient-button blue${(selectedFile === null) ? ' disabled' : ''}`}
        onClick={handleUpload}
        disabled={selectedFile === null}
      >
        Upload File
      </button>

    </div>
  );
}

export default FileUpload;
