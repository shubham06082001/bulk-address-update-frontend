import React, { useState, useEffect, useRef } from "react";
import { updateAddresses } from "../api/api";
import "./styles.css";

function TextBoxUpdate() {
  const [addresses, setAddresses] = useState(
    `[{
        "name": "hello Doe",
        "street": "123 Main St",
        "city": "New York",
        "state": "NY",
        "zip": "10001"
    },
    {
        "name": "Jane Doe",
        "street": "123 Main St",
        "city": "New York",
        "state": "NY",
        "zip": "10001"
    }]`
  );

  const [isValidJSON, setIsValidJSON] = useState(true);
  const [validationMessage, setValidationMessage] = useState("");

  const textareaRef = useRef(null);

  useEffect(() => {
    if (textareaRef.current) {
      const textarea = textareaRef.current;
      const lineCount = textarea.value.split("\n").length;
      textarea.rows = lineCount;
    }
  }, [addresses]);

  const handleAddressChange = (event) => {
    setAddresses(event.target.value);
    validateJSON(event.target.value);
  };

  const validateJSON = (jsonString) => {
    try {
      JSON.parse(jsonString);
      setIsValidJSON(true);
      setValidationMessage("");
    } catch (error) {
      setIsValidJSON(false);
      setValidationMessage("Invalid JSON format");
    }
  };

  const handleFormatJSON = () => {
    try {
      const parsedAddresses = JSON.parse(addresses);
      const formattedAddresses = JSON.stringify(parsedAddresses, null, 2);
      setAddresses(formattedAddresses);
      setIsValidJSON(true);
      setValidationMessage("");
    } catch (error) {
      console.error("Error formatting JSON:", error);
    }
  };

  const handleUpdateAddresses = async () => {
    if (!isValidJSON) {
      return;
    }

    try {
      const parsedAddresses = JSON.parse(addresses);
      const response = await updateAddresses(parsedAddresses);

      if (response) {
        console.log("Addresses updated successfully:", response);
        window.location.reload();
      }
    } catch (error) {
      console.error("Error updating addresses:", error);
    }
  };

  return (
    <div>
      <textarea
        ref={textareaRef}
        placeholder="Enter Addresses in JSON format"
        value={addresses}
        onChange={handleAddressChange}
        className={isValidJSON ? "valid-json" : "invalid-json"}
      ></textarea>
      {!isValidJSON ? (
        <div className="invalid-json">
          <div className="warning-icon">⚠️</div> Invalid JSON format:{" "}
          {validationMessage}
        </div>
      ) : null}
      <div className="button-container">
        <button
          className={`gradient-button green ${
            isValidJSON ? "" : "disabled-button"
          }`}
          onClick={handleFormatJSON}
        >
          Format JSON
        </button>
        <button
          className={`gradient-button blue ${
            isValidJSON ? "" : "disabled-button"
          }`}
          onClick={handleUpdateAddresses}
          disabled={!isValidJSON}
        >
          Update Addresses
        </button>
      </div>
    </div>
  );
}

export default TextBoxUpdate;
