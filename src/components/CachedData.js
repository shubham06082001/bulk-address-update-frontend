import React, { useState, useEffect } from "react";
import {
  getCachedData,
  refreshCachedData,
  clearCachedData,
  getMongoDBData,
} from "../api/api";
import "./styles.css";
import ClearMongoDBData from "./ClearMongoDBData";

function CachedData() {
  const [cachedAddresses, setCachedAddresses] = useState([]);
  const [mongoDBAddresses, setMongoDBAddresses] = useState([]);

  const handleClearAllCachedData = async () => {
    try {
      await clearAllCachedData();
      setCachedAddresses([]);
    } catch (error) {
      console.error("Error clearing all cached data:", error);
    }
  }

  const clearAllCachedData = async () => {
    try {
      const response = await clearCachedData();
      if (response.status === 200) {
        console.log("All cached data cleared successfully");
      } else {
        throw new Error("Failed to clear all cached data");
      }
    } catch (error) {
      throw error;
    }
  }

  const fetchCachedData = async () => {
    try {
      const cachedData = await getCachedData();
      if (cachedData && cachedData.addresses) {
        setCachedAddresses(cachedData.addresses);
      }
    } catch (error) {
      console.error("Error fetching cached data:", error);
    }
  }

  const fetchMongoDBData = async () => {
    try {
      const mongoData = await getMongoDBData();
      setMongoDBAddresses(mongoData.addresses);
    } catch (error) {
      console.error("Error fetching MongoDB data:", error);
    }
  }

  // Function to refresh cached data
  const handleRefresh = async () => {
    try {
      await refreshCachedData();
      fetchCachedData(); // Fetch fresh cached data after refresh
    } catch (error) {
      console.error("Error refreshing cached data:", error);
    }
  }

  useEffect(() => {
    // Fetch initial cached and MongoDB data
    fetchCachedData();
    fetchMongoDBData();
  }, []);

  return (
    <div className="container">
      <div className="data-box" id="cached-data-box">
        <h2>Cached Address Data</h2>
        <hr />
        <table className="data-table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Street</th>
              <th>City</th>
              <th>State</th>
              <th>Zip</th>
            </tr>
          </thead>
          <tbody>
            {cachedAddresses.map((address, index) => (
              <tr key={index}>
                <td>{address.name}</td>
                <td>{address.street}</td>
                <td>{address.city}</td>
                <td>{address.state}</td>
                <td>{address.zip}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="button-container">
          <button className="gradient-button blue" onClick={handleRefresh}>Add Sample Data in Redis Cache</button>
          <button className="gradient-button red" onClick={handleClearAllCachedData}>Clear All Cached Data</button>
        </div>
      </div>
      <div className="data-box" id="mongodb-data-box">
        <h2>MongoDB Address Data</h2>
        <hr />
        <table className="data-table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Street</th>
              <th>City</th>
              <th>State</th>
              <th>Zip</th>
            </tr>
          </thead>
          <tbody>
            {mongoDBAddresses.map((address, index) => (
              <tr key={index}>
                <td>{address.name}</td>
                <td>{address.street}</td>
                <td>{address.city}</td>
                <td>{address.state}</td>
                <td>{address.zip}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="button-container">
          <ClearMongoDBData /></div>
      </div>
    </div>
  );
}

export default CachedData;
