import React, { useState } from "react"

function ApiCallTracker({ apiName, apiFunction }) {
  const [elapsedTime, setElapsedTime] = useState(null)
  const [isRunning, setIsRunning] = useState(false)

  const handleApiCall = async () => {
    setIsRunning(true)
    const startTime = new Date()
    try {
      await apiFunction() // Invoke the provided API function
    } catch (error) {
      console.error(`Error making ${apiName} API call:`, error)
    } finally {
      const endTime = new Date()
      const timeTaken = endTime - startTime
      setElapsedTime(timeTaken)
      setIsRunning(false)
    }
  }

  return (
    <div>
      <button onClick={handleApiCall} disabled={isRunning}>
        {isRunning ? "Running..." : `Run ${apiName} API`}
      </button>
      {elapsedTime !== null && (
        <div>{`${apiName} API took ${elapsedTime} milliseconds`}</div>
      )}
    </div>
  )
}

export default ApiCallTracker
