import React from "react"
import { clearAllMongoDBData, clearCachedData } from "../api/api"
import "./styles.css"

function ClearMongoDBData() {
  const handleClearMongoDBData = async () => {
    try {
      const res = await clearCachedData()
      if (res.status === 200) {
        console.log("Redis Cache cleared successfully")
      } else {
        console.error("Failed to clear Redis Cache")
      }
      const response = await clearAllMongoDBData()
      if (response.status === 200) {
        console.log("MongoDB data cleared successfully")
      } else {
        console.error("Failed to clear MongoDB data")
      }
      window.location.reload()
    } catch (error) {
      console.error("Error clearing MongoDB data:", error)
    }
  }

  return (
    <div>
      <button className="gradient-button red" onClick={handleClearMongoDBData}>Clear MongoDB Data</button>
    </div>
  )
}

export default ClearMongoDBData
