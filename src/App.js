import React from "react";
import "./App.css";
import FileUpload from "./components/FileUpload";
import CachedData from "./components/CachedData";
import TextBoxUpdate from "./components/TextBoxUpdate";

function App() {
  const handleFileUpload = (file) => {
    console.log("Uploading file:", file.name);
  };

  return (
    <div className="App">
      {/* <h1>BULK ADDRESS UPDATE</h1> */}

      <hr />
      {/* FILE UPLOAD - ADDRESS UPDATE */}
      <FileUpload onFileUpload={handleFileUpload} />
      <hr />

      {/* TETBOX UPDATE ADDRESS */}
      <TextBoxUpdate />
      <hr />
      {/* MONGODB DATA */}

      {/* REDIS DATA */}
      <CachedData />
      <hr />
    </div>
  );
}

export default App;
